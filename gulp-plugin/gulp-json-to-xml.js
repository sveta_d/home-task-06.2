'use strict';

var through = require('through2');
var PluginError = require('gulp-util').PluginError;
var greplace = require('gulp-util').replaceExtension;
var PLUGIN_NAME = 'gulp-json-to-xml';

module.exports = function () {

    function convert (file, encoding, callback) {
        if (file.isNull()) {
            return callback(null, file);
        }

        if (file.isStream()) {
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams not supported!'));
        } else if (file.isBuffer()) {
            file.contents = new Buffer(convertToXML(JSON.parse(file.contents)));
            file.path = greplace(file.path, '.xml');
        }

        callback(null, file);

        function convertToXML (object){
            return '<?xml version="1.0" encoding="UTF-8"?>\n' + tabFunction(converting('', object));

            function converting(prev, current){
                var retString;
                if(Array.isArray(current)){
                    retString = prev + '\n' + '<array>' + content(current) + '</array>';
                }else if(typeof current == 'object'){
                    retString = prev + '\n' + '<' + typeof current +'>' + content(current) +'\n</' +typeof current + '>';
                }else{
                    retString = prev + '\n'+'<' + typeof current +'>' + content(current) +'</' +typeof current + '>';
                }

                function content(current){
                    if(Array.isArray(current)){
                        return current.reduce(converting, '')+'\n';
                    }else if(typeof current == 'object'){
                        return Object.keys(current).reduce(function(prev, item){
                            return prev + attr(converting('', current[item]), item);
                        },'');
                    }else{
                        return current;
                    }
                }

                function attr(func, content){
                    return func.indexOf('>') == '-1' ? func :  func.substr(0,  func.indexOf('>')) + ' name="' + content + '"' + func.substring(func.indexOf('>'));
                }
                return retString;
            }

            function tabFunction (string){
                var openTag = 1;
                var indexArrayOpen = allIndexOf(string,'<');

                function allIndexOf(str, toSearch) {
                    var indices = [];
                    for (var pos = str.indexOf(toSearch); pos !== -1; pos = str.indexOf(toSearch, pos + 1)) {
                        indices.push({'index': pos});
                    }
                    return indices;
                }

                for(var i = 0; i < indexArrayOpen.length; i++){
                    if(string.charAt(indexArrayOpen[i]['index']+1) == '/'){
                        openTag--;
                        indexArrayOpen[i]['tabs'] = openTag;
                    }else{
                        indexArrayOpen[i]['tabs'] = openTag;
                        openTag++;
                    }
                }

                var newStr = string.split('<');
                newStr.shift(); //because first element of it array is ''

                // if it is close tag of string or number - delete tabs
                for(var numb = 0; numb < newStr.length; numb++){
                    if(newStr[numb].substr(0,2) =='/s' || newStr[numb].substr(0,2) == '/n'){
                        newStr[numb] = '<' + newStr[numb];
                    }else{
                        newStr[numb] = tabs(indexArrayOpen[numb]['tabs'])+ '<' + newStr[numb];
                    }
                }

                function tabs(quantity){
                    var newTabs = '';
                    if(quantity >= 1){
                        for(var i = 0; i < quantity; i++){
                            newTabs += '\t';
                        }
                        return newTabs;
                    }
                    return '';
                }
                return newStr.join('');
            }
        }
    }
    return through.obj(convert);
};
