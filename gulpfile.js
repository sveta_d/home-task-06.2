var gulp = require('gulp');
var jsontoxml = require('./gulp-plugin/gulp-json-to-xml');

gulp.task('build:js', function () {
    return gulp.src('./src/js/*.json')
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('jsontoxml', function () {
    return gulp.src('./src/**/*.json')
        .pipe(jsontoxml())
        .pipe(gulp.dest('./dist/'));
});

gulp.task('default', ['jsontoxml']);